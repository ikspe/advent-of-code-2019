isVerbose = False

def calculate_fuel_part01( mass ) :
    """
    Fuel required to launch a given module is based on its mass.
    Specifically, to find the fuel required for a module, take its mass,
        divide by three, round down, and subtract 2."""
    return (int(mass / 3)) - 2

def calculate_fuel_part02( mass ) :
    """
    Fuel required to launch a given module is based on its mass.
    Specifically, to find the fuel required for a module, take its mass,
        divide by three, round down, and subtract 2."""
    global isVerbose
    fuel_mass = calculate_fuel_part01(mass)
    if isVerbose:
        print("[mass = ", mass, "] => fuel_mass = ", fuel_mass)
    if fuel_mass > 0:
        fuel_mass_for_fuel = calculate_fuel_part02(fuel_mass)
        if isVerbose:
            print("[mass = ", mass, "] => fuel_mass_for_fuel = ", fuel_mass_for_fuel)
        fuel_mass += fuel_mass_for_fuel
    if isVerbose:
        print("[mass = ", mass, "] => fuel_mass total = ", fuel_mass)
    return (fuel_mass if fuel_mass > 0 else 0)


def do_tests():
    for i in [14 , 1969 , 100756]:
        fuel01 = calculate_fuel_part01(i)
        print("part01. mass of ", i, " => fuel = ", fuel01)
        fuel02 = calculate_fuel_part02(i)
        print("part02. mass of ", i, " => fuel = ", fuel02)


def do_real_calculation():
    global isVerbose
    sum_fuel_int_01 = 0
    sum_fuel_float_01 = 0
    sum_fuel_int_02 = 0
    sum_fuel_float_02 = 0
    with open("input.txt") as file: # Use file to refer to the file object
        for line in file:
            if isVerbose:
                print(line)
                print(int(line) + 1)
            sum_fuel_int_01 += calculate_fuel_part01(int(line))
            sum_fuel_float_01 += calculate_fuel_part01(float(line))
            sum_fuel_int_02 += calculate_fuel_part02(int(line))
            sum_fuel_float_02 += calculate_fuel_part02(float(line))
    print("part01 : sum_fuel_int   = ", sum_fuel_int_01)
    print("part01 : sum_fuel_float = ", sum_fuel_float_01)
    print("part02 : sum_fuel_int   = ", sum_fuel_int_02)
    print("part02 : sum_fuel_float = ", sum_fuel_float_02)


def main():
    global isVerbose
    isVerbose = False
    # do_tests()
    do_real_calculation()


if __name__ == "__main__":
    # execute only if run as a script
    main()
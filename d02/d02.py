
input_tests = ["1,0,0,0,99", "2,3,0,3,99", "2,4,4,5,99,0", "1,1,1,4,99,5,6,0,99"]


def create_list_from_input(str_input):
    return [int(x) for x in str_input.split(',')]


def run_program(program):
    pos = 0
    while True:
        op_code = program[pos]
        if op_code == 99:
            # print("OK, finished program. Output = ", program)
            return True
        val1, val2, val3 = program[pos+1],  program[pos+2],  program[pos+3]
        if op_code == 1:
            program[val3] = program[val1] + program[val2]
        elif op_code == 2:
            program[val3] = program[val1] * program[val2]
        else:
            print("ERROR, unknown op_code = ", op_code, " at pos ", pos)
            return False
        pos += 4
    print("ERROR, unexpected end of program at pos ", pos)
    return False

def do_tests():
    global input_tests
    for str_i in input_tests:
        tmp_program = create_list_from_input(str_i)
        if not run_program(tmp_program):
            print("ERROR on input", str_i)
        else:
            print(str_i, " => ", tmp_program)


def do_calculation_common(noun, verb):
    with open("input.txt") as file: # Use file to refer to the file object
        str_i = file.readline()
        tmp_program = create_list_from_input(str_i)
        tmp_program[1] = noun
        tmp_program[2] = verb
        if not run_program(tmp_program):
            print("ERROR on input", str_i)
        else:
            # print("SUCCESS on input", str_i, " ==> ")
            # print(tmp_program)
            # print("RESULT : ", tmp_program[0])
            return tmp_program[0]


def do_calculation_part1():
    """
    Once you have a working computer, the first step is to restore the gravity assist program (your puzzle input)
    to the "1202 program alarm" state it had just before the last computer caught fire.
    To do this, before running the program, replace position 1 with the value 12
    and replace position 2 with the value 2. What value is left at position 0 after the program halts?
    """
    output = do_calculation_common(12, 2)
    print("RESULT = ", output)


def do_calculation_part2():
    """
    Once the program has halted, its output is available at address 0, also just like before.
    Each time you try a pair of inputs, make sure you first reset the computer's memory to the values in the program (your puzzle input) -
    in other words, don't reuse memory from a previous attempt.
    Find the input noun and verb that cause the program to produce the output 19690720.
    What is 100 * noun + verb? (For example, if noun=12 and verb=2, the answer would be 1202.)
    """
    for noun in range(0, 100):
        for verb in range(0, 100):
            output = do_calculation_common(noun, verb)
            if output == 19690720:
                print("FOUND : output = ", output, " 100 * noun + verb = ", 100 * noun + verb)
                return


def main():
    # do_tests()
    do_calculation_part1()
    do_calculation_part2()
    print(list(range(0, 100)))

if __name__ == "__main__":
    # execute only if run as a script
    main()
test_inputs1 = ["R8,U5,L5,D3",
                "U7,R6,D4,L4"]
test_inputs2 = ["R75,D30,R83,U83,L12,D49,R71,U7,L72",
                "U62,R66,U55,R34,D71,R55,D58,R83"]
test_inputs3 = ["R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51",
                "U98,R91,D20,R16,D67,R40,U7,R15,U6,R7"]

test_use_cases_xavier = ["R6,D3,R7,U7,L3,D7"]
test_inputs_xavier_2 = ["R75,D30,R83,U83,L12,D49,R71,U7,L72"]


class Grid2:

    def __init__(self, width, height, x_begin_param, y_begin_param):
        self.the_grid = [['.' for _ in range(0, width)] for _ in range(0, height)]  # empty grid at beginning
        self.x_begin = x_begin_param
        self.y_begin = y_begin_param
        self.x_current = self.x_begin
        self.y_current = self.y_begin
        self.the_grid[self.y_begin][self.x_begin] = 'o'
        self.list_intersections = []  # list of : [x_inters, y_inters, distance_inters]
        self.list_other_grids = []
        self.number_of_steps = 0

    @staticmethod
    def find_instr_min_max(str_list_instr):
        list_instr = str_list_instr.split(',')
        x_cur, y_cur = 0, 0
        x_min, x_max = x_cur, x_cur
        y_min, y_max = y_cur, y_cur
        # print("find_instr_min_max() : list_instr = ", list_instr)
        for instr in list_instr:
            direction = instr[0]
            length = int(instr[1:])
            if direction == 'R':
                x_cur += length
            elif direction == 'L':
                x_cur -= length
            elif direction == 'U':
                y_cur -= length
            elif direction == 'D':
                y_cur += length
            else:
                raise Exception("Unknown direction : " + direction + " (instruction = " + instr + ")")
            x_min = min(x_min, x_cur)
            x_max = max(x_max, x_cur)
            y_min = min(y_min, y_cur)
            y_max = max(y_max, y_cur)
        return x_min, x_max, y_min, y_max

    @staticmethod
    def find_all_wires_min_max(list_all_wires_instr):
        list_x_min, list_x_max, list_y_min, list_y_max = [], [], [], []
        for one_wire_list_instr in list_all_wires_instr:
            x_min, x_max, y_min, y_max = Grid2.find_instr_min_max(one_wire_list_instr)
            # print("x_min, x_max, y_min, y_max = ", x_min, x_max, y_min, y_max)
            list_x_min.append(x_min)
            list_x_max.append(x_max)
            list_y_min.append(y_min)
            list_y_max.append(y_max)
        # print("find_all_wires_min_max() : list_x_min, list_x_max, list_y_min, list_y_max = ", list_x_min, list_x_max, list_y_min, list_y_max)
        return min(list_x_min), max(list_x_max), min(list_y_min), max(list_y_max)

    @staticmethod
    def find_all_wires_dimensions(list_all_wires_instr):
        x_min, x_max, y_min, y_max = Grid2.find_all_wires_min_max(list_all_wires_instr)
        # print("find_all_wires_dimensions() : global(x_min, x_max, y_min, y_max) = ", x_min, x_max, y_min, y_max)
        width  = x_max - x_min + 1
        height = y_max - y_min + 1
        x_begin = 0 - x_min
        y_begin = 0 - y_min
        # print("find_all_wires_dimensions() : x_begin, y_begin, width, height = ", x_begin, y_begin, width, height)
        return x_begin, y_begin, width, height

    def print_grid(self):
        for line in self.the_grid:
            str_tmp = ''.join(line)
            print(str_tmp)
        pass

    def get_current_char(self):
        return self.the_grid[self.y_current][self.x_current]

    def set_current_char(self, new_char):
        self.the_grid[self.y_current][self.x_current] = new_char

    def add_reference_grid(self, other_grid):
        self.list_other_grids.append(other_grid)
        pass

    def register_intersection(self, x, y, verbose):
        distance = abs(x - self.x_begin) + abs(y - self.y_begin)
        self.list_intersections.append([x, y, distance, self.number_of_steps])
        if verbose:
            print("found intersection at ", x, y, " : distance = ", distance)

    def is_current_an_existing_intersection(self):
        for iter_intersection in self.list_intersections:
            if self.x_current == iter_intersection[0] and self.y_current == iter_intersection[1]:
                return True
        return False

    def execute_all_instr(self, str_list_instr, stop_at_intersection):
        list_instr = str_list_instr.split(',')
        for instr in list_instr:
            self.execute_instr(instr, stop_at_intersection)
            if stop_at_intersection:
                if self.is_current_an_existing_intersection():
                    return  # end of execution
        pass

    def execute_instr(self, instr, stop_at_intersection):
        direction = instr[0]
        length = int(instr[1:])
        new_char = '|' if (direction == 'U' or direction == 'D') else '_'
        if self.get_current_char() == '|' or self.get_current_char() == '_':
            self.set_current_char('x')  # intersection of a single wire. Do not override begin pos : 'o'
        for _ in range(0, length):
            if direction == 'R':
                self.x_current += 1
            elif direction == 'L':
                self.x_current -= 1
            elif direction == 'U':
                self.y_current -= 1
            elif direction == 'D':
                self.y_current += 1
            else:
                raise Exception("Unknown direction : " + direction + " (instruction = " + instr + ")")
            old_char = self.get_current_char()
            self.number_of_steps += 1
            if stop_at_intersection:
                if self.is_current_an_existing_intersection():
                    return  # end of execution
            if (self.x_current != self.x_begin) and (self.y_current != self.y_begin):
                if self.get_current_char() == '|' or self.get_current_char() == '_':
                    self.set_current_char('x')  # intersection of a single wire
                else:
                    self.set_current_char(new_char)
                for grid in self.list_other_grids:
                    if grid.the_grid[self.y_current][self.x_current] != '.':
                        self.register_intersection(self.x_current, self.y_current, False)
        pass
    pass


def execute_wires_grid2(list_all_wires_instr):
    # create grids
    x_begin, y_begin, width, height = Grid2.find_all_wires_dimensions(list_all_wires_instr)
    list_of_grids = []
    for list_instr in list_all_wires_instr:
        my_grid = Grid2(width, height, x_begin, y_begin)
        for prev_grid in list_of_grids:
            my_grid.add_reference_grid(prev_grid)
        my_grid.execute_all_instr(list_instr, False)  # False : don't stop at intersections
        list_of_grids.append(my_grid)
        # my_grid.print_grid()
    all_intersections = list_of_grids[-1].list_intersections
    print("all intersections : ", all_intersections)
    if len(all_intersections) == 0:
        print("NO INTERSECTION => no minimum distance")
        return
    list_distances = [inter[2] for inter in all_intersections]
    print("all distances intersections : ", list_distances)
    min_distance = min(list_distances)
    print("Minimum Manhattan distance intersections : ", min_distance)

    """
    # restart with empty grids : they will be computed to find min number of steps for total of wires
    list_of_grids = []
    list_of_number_of_steps = []
    for this_intersection in all_intersections:
        total_all_grids_number_steps = 0
        for list_instr in list_all_wires_instr:
            my_grid = Grid2(width, height, x_begin, y_begin)
            my_grid.register_intersection(this_intersection[0], this_intersection[1], False)  # False : not verbose
            my_grid.execute_all_instr(list_instr, True)  # True : stop at intersections
            print("Number of steps for this grid to reach intersection = ", my_grid.number_of_steps)
            total_all_grids_number_steps += my_grid.number_of_steps
        list_of_number_of_steps.append(total_all_grids_number_steps)
    print("list_of_number_of_steps = ", list_of_number_of_steps)
    print("min number of steps = ", min(list_of_number_of_steps))
    """

    # do the calculation in reverse order : wire 2 first , then wire 1. And add number of steps
    list_of_grids = []
    list_all_wires_instr.reverse()
    for list_instr in list_all_wires_instr:
        my_grid = Grid2(width, height, x_begin, y_begin)
        for prev_grid in list_of_grids:
            my_grid.add_reference_grid(prev_grid)
        my_grid.execute_all_instr(list_instr, False)  # False : don't stop at intersections
        list_of_grids.append(my_grid)
        # my_grid.print_grid()
    reverse_all_intersections = list_of_grids[-1].list_intersections
    print("reverse all intersections : ", all_intersections)
    if len(reverse_all_intersections) == 0:
        print("NO INTERSECTION => no minimum distance")
        return
    reverse_list_distances = [inter[2] for inter in reverse_all_intersections]
    print("all distances intersections : ", reverse_list_distances)
    reverse_min_distance = min(reverse_list_distances)
    print("Minimum Manhattan distance intersections : ", reverse_min_distance)

    map_number_of_steps = {}
    list_number_of_steps = []
    for it_inter in all_intersections:
        found = False
        x, y = it_inter[0], it_inter[1]
        for it_rev_inter in reverse_all_intersections:
            if x == it_rev_inter[0] and y == it_rev_inter[1]:
                found = True
                total_steps = it_inter[3] + it_rev_inter[3]
                map_number_of_steps[(x, y)] = total_steps
                list_number_of_steps.append(total_steps)
                break
        if not found:
            print("ERROR, impossible to find reverse for x, y =", x, y)
            return
    print("map_number_of_steps = ", map_number_of_steps)
    print("list_number_of_steps = ", list_number_of_steps)
    print("min(list_number_of_steps) = ", min(list_number_of_steps))



def do_tests():
    # list_tests = [test_inputs1]
    list_tests = [test_inputs1, test_inputs2, test_inputs3]
    # list_tests = [test_use_cases_xavier]
    # list_tests = [test_inputs2]
    # list_tests = [test_inputs_xavier_2]
    for it_test in list_tests:
        print("do_tests() : ", it_test)
        execute_wires_grid2(it_test)


def do_calculation():
    with open("input.txt") as file:  # Use file to refer to the file object
        list_all_wires_instr = []
        line = file.readline()
        while line:
            list_all_wires_instr.append(line)
            line = file.readline()
        print("do_calculation() : ", list_all_wires_instr)
        execute_wires_grid2(list_all_wires_instr)


def main():
    do_tests()
    do_calculation()
    # do_calculation_part1()
    # do_calculation_part2()
    # print(list(range(0, 100)))


if __name__ == "__main__":
    # execute only if run as a script
    main()


input_tests_d02 = [("1,0,0,0,99", "2,0,0,0,99"),
                   ("2,3,0,3,99", "2,3,0,6,99"),
                   ("2,4,4,5,99,0", "2,4,4,5,99,9801"),
                   ("1,1,1,4,99,5,6,0,99", "30,1,1,4,2,5,6,0,99")]
input_tests_d05_test01_in_out = "3,0,4,0,99"
input_tests_d05_test02_parameter_mode = [("1002,4,3,4,33", "1002,4,3,4,99"),
                                         ("1101,100,-1,4,0", "1101,100,-1,4,99")]
# next tests : program ; list of tests : inputs + expected outputs
input_tests_d05_test03_compare = [("3,9,8,9,10,9,4,9,99,-1,8", [(8,1), (0,0), (1,0), (9,0), (-1000, 0), (13337,0)]),         # equal to 8
                                  ("3,9,7,9,10,9,4,9,99,-1,8", [(0,1), (1,1), (7,1), (-1000, 1), (8,0), (9,0), (13337,0)]),  # less than 8
                                  ("3,3,1108,-1,8,3,4,3,99",   [(8,1), (0,0), (1,0), (9,0), (-1000, 0), (13337,0)]),         # equal to 8
                                  ("3,3,1107,-1,8,3,4,3,99",   [(0,1), (1,1), (7,1), (-1000, 1), (8,0), (9,0), (13337,0)])]  # less than 8
input_tests_d05_test04_jump = [("3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9", [(0,0), (1,1), (2,1), (9,1), (-1000, 1), (13337,1)]),  # if input == 0
                               ("3,3,1105,-1,9,1101,0,0,12,4,12,99,1", [(0,0), (1,1), (2,1), (9,1), (-1000, 1), (13337,1)])]       # if input == 0

input_tests_d05_test05_full = [("3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31," +   # test < 8 ; == 8 ; > 8
                                "1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104," +
                                "999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99",
                                [(0, 999), (1, 999), (-1, 999), (7, 999), (8, 1000), (9, 1001), (10, 1001), (1337, 1001)])]


"""
this is basically the d02 IntCode computer modified to accept new instructions
"""


class IntCodeComputer:

    def __init__(self, yield_on_output=False, is_increase_memory=False):
        self.is_verbose = False
        self.program = []
        self.is_finished = False
        self.pos_in_program = 0
        self.pos_input = 0
        self.dict_operand_count = {1: 3,
                                   2: 3,
                                   99: 0,
                                   3: 1,
                                   4: 1,
                                   5: 2,
                                   6: 2,
                                   7: 3,
                                   8: 3,
                                   9: 1}
        self.yield_on_output = yield_on_output  # d07 : yield_on_output => run() will return on output, even if not reached 99
        self.input_values = []
        self.output_values = []
        self.relative_base = 0  # relative mode, introduced on d09
        self.is_increase_memory = is_increase_memory  # realloc memory if requested, introduced on d09
        self.is_verbose_relative_base = False

    def create_program(self, str_input, input_values, output_values):
        self.program = [int(x) for x in str_input.split(',')]
        self.input_values = input_values
        self.output_values = output_values
        return self.program

    def add_input(self, input_value):
        self.input_values.append(input_value)

    def get_memory_at_position(self, pos_idx):
        self.realloc_memory_if_requested_and_allowed(pos_idx)
        return self.program[pos_idx]

    def set_memory_at_position(self, pos_idx, value):
        self.realloc_memory_if_requested_and_allowed(pos_idx)
        self.program[pos_idx] = value

    def run_computer(self):
        if self.is_verbose:
            print("Debug,                          ; program = ", self.program, ", input_values = ", self.input_values, " pos = ", self.pos_in_program, " , pos_input = ", self.pos_input)
        local_cond_continue_exec = True
        while local_cond_continue_exec:
            op_code_full_with_param_modes = self.program[self.pos_in_program]
            op_code = op_code_full_with_param_modes % 100
            if self.is_verbose:
                print("Debug, pos = ", self.pos_in_program, ", op_code = ", op_code, " ; program = ", self.program)
            if op_code not in self.dict_operand_count:
                print("ERROR : unknown op_code ", op_code, ": not in dict_operand_count = ", self.dict_operand_count, ", pos = ", self.pos_in_program)
                return False
            operand_count = self.dict_operand_count[op_code]
            # operand_list = []
            operand_list = self.program[self.pos_in_program+1:self.pos_in_program+operand_count+1]  # parameter_mode = position
            operand_list_param_mode = [x for x in operand_list]
            # for each operand, check its parameter_mode :
            all_parameters_mode = int(op_code_full_with_param_modes / 100)
            # if all_parameters_mode > 0:
            #    print("pos_in_program = ", self.pos_in_program, " op_code_full_with_param_modes = ", op_code_full_with_param_modes, ", all_parameters_mode = ", all_parameters_mode)
            for i in range(0, operand_count):
                cur_param_mode = (all_parameters_mode % 10)
                all_parameters_mode = int(all_parameters_mode / 10)
                # print("Debug, cur_param_mode = ", cur_param_mode, " at pos_in_program = ", self.pos_in_program, ", op_code = ", op_code, ", i = ", i)
                # if i == 0 and cur_param_mode != 0:
                #    print("ERROR. i == ", i, " and cur_param_mode == ", cur_param_mode,
                #          ". This is not supposed to happen : Parameters that an instruction writes to will never be in immediate mode.")
                #    return False
                if cur_param_mode == 0:
                    operand_list_param_mode[i] = self.get_memory_at_position(operand_list[i])  # position mode (default mode, from d02)
                elif cur_param_mode == 1:
                    operand_list_param_mode[i] = operand_list[i]  # immediate mode (introduced on d05)
                elif cur_param_mode == 2:
                    operand_list_param_mode[i] = self.get_memory_at_position(self.relative_base + operand_list[i])  # relative mode (introduced on d09)
                    operand_list[i] = self.relative_base + operand_list[i]  # found because of use case = 203 on d09 part1
                else:
                    print("Error, unknown parameter_mode = ", cur_param_mode, " at pos = ", self.pos_in_program, ", op_code = ", op_code, ", i = ", i)
                    return False
            if self.is_verbose:
                print("Debug, pos = ", self.pos_in_program, ", op_code = ", op_code, ", operand_list = ", operand_list)
                print("Debug, pos = ", self.pos_in_program, ", op_code = ", op_code, ", operand_list_param_mode = ", operand_list_param_mode)
            increase_position_ptr = True
            if op_code == 99:
                # print("OK, finished program. Output = ", program)
                self.is_finished = True
                local_cond_continue_exec = False
            elif op_code == 1:
                # addition
                self.set_memory_at_position(operand_list[2], operand_list_param_mode[0] + operand_list_param_mode[1])
            elif op_code == 2:
                # multiply
                self.set_memory_at_position(operand_list[2], operand_list_param_mode[0] * operand_list_param_mode[1])
            elif op_code == 3:
                # input mode
                if self.is_verbose:
                    print("Debug input : op_code = ", op_code, " ; full_op_code_param = ", op_code_full_with_param_modes, " ; writing at : ", operand_list[0], " ; operand_list = ", operand_list, " ; operand_list_param_mode = ", operand_list_param_mode)
                self.set_memory_at_position(operand_list[0], self.input_values[self.pos_input])
                self.pos_input += 1
            elif op_code == 4:
                if self.is_verbose:
                    print("Debug, generating output = ", operand_list_param_mode[0])
                # output mode
                self.output_values.append(operand_list_param_mode[0])
                if self.yield_on_output:
                    local_cond_continue_exec = False
            elif op_code == 5:
                # jump-if-true
                if operand_list_param_mode[0] != 0:
                    self.pos_in_program = operand_list_param_mode[1]
                    increase_position_ptr = False
            elif op_code == 6:
                # jump-if-false
                if operand_list_param_mode[0] == 0:
                    self.pos_in_program = operand_list_param_mode[1]
                    increase_position_ptr = False
            elif op_code == 7:
                # less than
                if operand_list_param_mode[0] < operand_list_param_mode[1]:
                    self.program[operand_list[2]] = 1
                else:
                    self.program[operand_list[2]] = 0
            elif op_code == 8:
                # equals
                if operand_list_param_mode[0] == operand_list_param_mode[1]:
                    self.program[operand_list[2]] = 1
                else:
                    self.program[operand_list[2]] = 0
            elif op_code == 9:
                # adjusts the relative base (from d09)
                sav_previous_base = self.relative_base
                self.relative_base += operand_list_param_mode[0]
                if self.is_verbose_relative_base:
                    print("Debug. adjusting relative_base : previous = ", sav_previous_base, " , new = ", self.relative_base)
            else:
                print("ERROR, op_code ", op_code, " is no implemented...")
                return False
            if increase_position_ptr:
                self.pos_in_program += (operand_count + 1)
        return True  # return but maybe program is not finished (yield ?)

    def realloc_memory_if_requested_and_allowed(self, pos_idx) -> bool:
        if len(self.program) > pos_idx:
            return True  # no need to realloc
        if not self.is_increase_memory:
            print("ERROR, realloc memory IS NOT allowed")
            return False
        if self.is_verbose_relative_base:
            print("Debug : reallocating memory... Current size = ", len(self.program), " ; extending to size = ", pos_idx + 1)
        self.program.extend([0 for _ in range(len(self.program), pos_idx + 1)])
        return True

    pass  # class IntCodeComputer


def do_tests_d02():
    global input_tests_d02
    print("==== testing : input_tests_d02 ====")
    for it_one_test in input_tests_d02:
        str_input_program = it_one_test[0]
        expected_output = it_one_test[1]
        my_computer = IntCodeComputer()
        input_values = []
        output_values = []
        tmp_program = my_computer.create_program(str_input_program, input_values, output_values)
        if not my_computer.run_computer():
            print("ERROR on input", str_input_program)
        else:
            # print("tmp_program = ", tmp_program)
            str_out_program = ','.join([str(x) for x in tmp_program])
            if str_out_program == expected_output:
                print("SUCCESS : ", str_input_program, " => ", tmp_program, " = ", str_out_program, " ; expected output = ", expected_output)
            else:
                print("FAILURE : ", str_input_program, " => ", tmp_program, " = ", str_out_program, " ; expected output = ", expected_output)


def do_tests_d05_position_mode():
    print("==== testing : do_tests_d05_position_mode ====")
    for input_value in [0, 1, 2, 5, 42, 1337, -1000]:
        str_input_program = input_tests_d05_test01_in_out
        input_values = [input_value]
        output_values = []
        expected_output_value = [input_value]
        my_computer = IntCodeComputer()
        tmp_program = my_computer.create_program(str_input_program, input_values, output_values)
        if not my_computer.run_computer():
            print("ERROR on input", str_input_program)
        else:
            # print("tmp_program = ", tmp_program)
            if output_values == expected_output_value:
                print("SUCCESS : ", str_input_program, " => ", tmp_program, " => ", output_values, " ; expected output = ", expected_output_value)
            else:
                print("FAILURE : ", str_input_program, " => ", tmp_program, " => ", output_values, " ; expected output = ", expected_output_value)

    for it_one_test in input_tests_d05_test02_parameter_mode:
        str_input_program = it_one_test[0]
        expected_output = it_one_test[1]
        input_values = []
        output_values = []
        my_computer = IntCodeComputer()
        tmp_program = my_computer.create_program(str_input_program, input_values, output_values)
        if not my_computer.run_computer():
            print("ERROR on input", str_input_program)
        else:
            # print("tmp_program = ", tmp_program)
            str_out_program = ','.join([str(x) for x in tmp_program])
            if str_out_program == expected_output:
                print("SUCCESS : ", str_input_program, " => ", tmp_program, " = ", str_out_program, " ; expected output = ", expected_output)
            else:
                print("FAILURE : ", str_input_program, " => ", tmp_program, " = ", str_out_program, " ; expected output = ", expected_output)


def do_tests_d05_compare():
    print("==== testing : do_tests_d05_compare ====")
    for it_test in input_tests_d05_test03_compare:
        str_input_program = it_test[0]
        test_data = it_test[1]
        for it_test_data in test_data:
            input_values = [it_test_data[0]]
            output_values = []
            expected_output_value = [it_test_data[1]]
            my_computer = IntCodeComputer()
            tmp_program = my_computer.create_program(str_input_program, input_values, output_values)
            if not my_computer.run_computer():
                print("ERROR on input", str_input_program)
            else:
                # print("tmp_program = ", tmp_program)
                str_out_program = ','.join([str(x) for x in tmp_program])
                if output_values == expected_output_value:
                    print("SUCCESS : ", str_input_program, " => ", tmp_program, " = ", output_values, " ; expected output = ", expected_output_value)
                else:
                    print("FAILURE : ", str_input_program, " => ", tmp_program, " = ", output_values, " ; expected output = ", expected_output_value)


def do_tests_d05_jump_test():
    print("==== testing : do_tests_d05_jump_test ====")
    for it_test in input_tests_d05_test04_jump:
        str_input_program = it_test[0]
        test_data = it_test[1]
        for it_test_data in test_data:
            input_values = [it_test_data[0]]
            output_values = []
            expected_output_value = [it_test_data[1]]
            my_computer = IntCodeComputer()
            tmp_program = my_computer.create_program(str_input_program, input_values, output_values)
            if not my_computer.run_computer():
                print("ERROR on input", str_input_program)
            else:
                # print("tmp_program = ", tmp_program)
                str_out_program = ','.join([str(x) for x in tmp_program])
                if output_values == expected_output_value:
                    print("SUCCESS : ", str_input_program, " => ", tmp_program, " = ", output_values, " ; expected output = ", expected_output_value)
                else:
                    print("FAILURE : ", str_input_program, " => ", tmp_program, " = ", output_values, " ; expected output = ", expected_output_value)


def do_tests_d05_full():
    print("==== testing : do_tests_d05_full ====")
    for it_test in input_tests_d05_test05_full:
        str_input_program = it_test[0]
        test_data = it_test[1]
        for it_test_data in test_data:
            input_values = [it_test_data[0]]
            output_values = []
            expected_output_value = [it_test_data[1]]
            my_computer = IntCodeComputer()
            tmp_program = my_computer.create_program(str_input_program, input_values, output_values)
            if not my_computer.run_computer():
                print("ERROR on input", str_input_program)
            else:
                # print("tmp_program = ", tmp_program)
                str_out_program = ','.join([str(x) for x in tmp_program])
                if output_values == expected_output_value:
                    print("SUCCESS : ", str_input_program, " => ", tmp_program, " = ", output_values, " ; expected output = ", expected_output_value)
                else:
                    print("FAILURE : ", str_input_program, " => ", tmp_program, " = ", output_values, " ; expected output = ", expected_output_value)


def do_real_calculation_common(input_value):
    with open("input.txt") as file: # Use file to refer to the file object
        str_i = file.readline()
        input_values = [input_value]
        output_values = []
        my_computer = IntCodeComputer()
        tmp_program = my_computer.create_program(str_i, input_values, output_values)
        if not my_computer.run_computer():
            print("ERROR on input", str_i)
        else:
            # print("SUCCESS on input", str_i, " ==> ")
            # print(tmp_program)
            # print("RESULT : ", tmp_program[0])
            print("RESULT : ", output_values)
            return output_values


def do_real_calculation_part1():
    print("==== do_real_calculation_part1() ====")
    return do_real_calculation_common(1)


def do_real_calculation_part2():
    print("==== do_real_calculation_part2() ====")
    return do_real_calculation_common(5)


def do_tests_part1():
    do_tests_d02()
    do_tests_d05_position_mode()


def do_tests_part2():
    do_tests_d05_compare()
    do_tests_d05_jump_test()
    do_tests_d05_full()


def main():
    do_tests_part1()
    do_real_calculation_part1()
    do_tests_part2()
    do_real_calculation_part2()


if __name__ == "__main__":
    # execute only if run as a script
    main()
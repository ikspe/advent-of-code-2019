import itertools

from d05.d05 import IntCodeComputer

d07_tests01 = [("3,15,3,16,1002,16,10,16,1,16,15,15,4,15,99,0,0", (4,3,2,1,0), 43210),  # program ; setting sequences ; expected output
               ("3,23,3,24,1002,24,10,24,1002,23,-1,23,101,5,23,23,1,24,23,23,4,23,99,0,0", (0,1,2,3,4), 54321),
               ("3,31,3,32,1002,32,10,32,1001,31,-2,31,1007,31,0,33,1002,33,7,33,1,33,31,31,1,32,31,31,4,31,99,0,0,0", (1,0,4,3,2), 65210)]

d07_tests02 = [("3,26,1001,26,-4,26,3,27,1002,27,2,27,1,27,26,27,4,27,1001,28,-1,28,1005,28,6,99,0,0,5", (9,8,7,6,5), 139629729),
               ("3,52,1001,52,-5,52,3,53,1,52,56,54,1007,54,5,55,1005,55,26,1001,54," +
                "-5,54,1105,1,12,1,53,54,53,1008,54,0,55,1001,55,1,55,2,53,55,53,4," +
                "53,1001,56,-1,56,1005,56,6,99,0,0,0,0,10", (9,7,8,5,6), 18216)]


def do_tests_part1():
    print("==== do_tests_part1 ====")
    for it_test in d07_tests01:
        str_program = it_test[0]
        phase_sequence = it_test[1]
        expected_output = it_test[2]
        print("    ==== test input = ", str_program)
        last_output = 0  # input of first program is always 0 in this test
        for ampli_idx in range(0, len(phase_sequence)):
            ampli_value = phase_sequence[ampli_idx]
            input_values = [ampli_value, last_output]
            output_values = []
            my_computer = IntCodeComputer()
            my_computer.create_program(str_program, input_values, output_values)
            # print("        str_program = ", str_program, " => program = ", program)
            if not my_computer.run_computer():
                print("        ERROR for ampli_idx : ", ampli_idx)
                return
            # else:
            #    # print("        SUCCESS for ampli_idx : ", ampli_idx, " output = ", output_values)
            last_output = output_values[0]
        print("        => Found Max thruster signal ", last_output)
        if last_output != expected_output:
            print("    ERROR. Got Max thruster signal ", last_output, " ; expected ", expected_output)
        else:
            print("    SUCCESS. Got Max thruster signal ", last_output, " ; expected ", expected_output)


def do_real_calculation_part1():
    print("==== do_real_calculation_part1 ====")
    with open("input.txt") as file: # Use file to refer to the file object
        str_program = file.readline()
        list_all_possible_phase_sequences = itertools.permutations([x for x in range(0,5)], 5)
        max_out_value = []
        sequence_for_max_out_value = []
        for phase_sequence in list_all_possible_phase_sequences:
            # print(phase_sequence)
            last_output = 0  # input of first program is always 0 in this test
            for ampli_value in phase_sequence: # five amplis : A,B,C,D,E
                # print("    str_program = ", str_program, " => program = ", program)
                input_values = [ampli_value, last_output]
                output_values = []
                # print("    input_values = ", input_values)
                my_computer = IntCodeComputer()
                my_computer.create_program(str_program, input_values, output_values)
                if not my_computer.run_computer():
                    print("    ERROR for ampli")
                    return
                last_output = output_values[0]
                # print("    output_values = ", output_values)
            if (len(max_out_value) == 0) or (max_out_value[0] < last_output):
                # print("    Found a max : output = ", last_output, ", for sequence = ", phase_sequence)
                max_out_value = [last_output]
                sequence_for_max_out_value = phase_sequence
        print("Global max_out_value = ", max_out_value, " for phase sequence = ", sequence_for_max_out_value)
        expected_output = [338603]  # we know this is the expected result... as we have already passed it
        if max_out_value == expected_output:
            print("SUCCESS of program : computed_output = ", max_out_value, " same as expected_output")
        else:
            print("ERROR : success of program : but incorrect computed_output = ", max_out_value, " : expected_output = ", expected_output)



def exec_part2_common(str_program, phase_sequence):
    # print("    ==== exec_part2_common() ; str_program = ", str_program)
    list_computer_ampli = []
    for ampli_idx in range(0, len(phase_sequence)):
        ampli_value = phase_sequence[ampli_idx]
        input_values = [ampli_value]
        output_values = []
        # print("Info, creating program for ampli idx = ", ampli_idx, ", phase_sequence = ", phase_sequence, ", input_values = ", input_values)
        my_computer = IntCodeComputer(yield_on_output=True)  # yield_on_output => run() will return on output, even if not reached 99
        # my_computer.is_verbose = True
        program = my_computer.create_program(str_program, input_values, output_values)
        list_computer_ampli.append(my_computer)

    has_finished = False
    debug_count_iter = 0
    last_output = 0  # first ampli takes 0 ; then connecting to the next one ; and feedback loop
    while not has_finished:
        for ampli_idx in range(0, len(phase_sequence)):
            list_computer_ampli[ampli_idx].add_input(last_output)
            # print("INFO : computer[ampli_idx = ", ampli_idx, "].run_computer() will be called... input_values[ = ", list_computer_ampli[ampli_idx].input_values)
            if not list_computer_ampli[ampli_idx].run_computer():
                print("ERROR : run_computer() FAILED")
                return
            last_output = list_computer_ampli[ampli_idx].output_values[-1]
            # print("Info : computer[ampli_idx = ", ampli_idx, "].run_computer() succeed ; output = ", list_computer_ampli[ampli_idx].output_values[-1])
            if list_computer_ampli[ampli_idx].is_finished:
                # print("INFO : computer[ampli_idx = ", ampli_idx, "] is_finished")
                has_finished = True  # will end : will take output of ampli E

        debug_count_iter += 1
        if debug_count_iter >= 2000:
            print("DEBUG : ERROR? stopping... : debug_count_iter = ", debug_count_iter)
            break

    return list_computer_ampli[-1].output_values[-1]


def do_tests_part2():
    print("==== do_tests_part2 ====")
    for it_test in d07_tests02:
        str_program = it_test[0]
        phase_sequence = it_test[1]
        expected_output = it_test[2]
        computed_output = exec_part2_common(str_program, phase_sequence)

        if computed_output != expected_output:
            print("    ERROR. Got Max thruster signal ", computed_output, " ; expected ", expected_output)
        else:
            print("    SUCCESS. Got Max thruster signal ", computed_output, " ; expected ", expected_output)


def do_real_calculation_part2():
    print("==== do_real_calculation_part2 ====")
    with open("input.txt") as file: # Use file to refer to the file object
        str_program = file.readline()
        list_all_possible_phase_sequences = itertools.permutations([x for x in range(5,10)], 5)
        max_out_value = []
        sequence_for_max_out_value = []
        for phase_sequence in list_all_possible_phase_sequences:
            # print(phase_sequence)
            computed_output = exec_part2_common(str_program, phase_sequence)
            if (len(max_out_value) == 0) or (max_out_value[0] < computed_output):
                # print("    Found a max : output = ", computed_output, ", for sequence = ", phase_sequence)
                max_out_value = [computed_output]
                sequence_for_max_out_value = phase_sequence
        print("Global max_out_value = ", max_out_value, " for phase sequence = ", sequence_for_max_out_value)
        expected_output = [63103596]  # we know this is the expected result... as we have already passed it
        if max_out_value == expected_output:
            print("SUCCESS of program : computed_output = ", max_out_value, " same as expected_output")
        else:
            print("ERROR : success of program : but incorrect computed_output = ", max_out_value, " : expected_output = ", expected_output)


def main():
    do_tests_part1()
    do_real_calculation_part1()
    do_tests_part2()
    do_real_calculation_part2()


if __name__ == "__main__":
    # execute only if run as a script
    main()


d08_test_part1 = [(3, 2, "123456789012")]
d08_test_part2 = [(2, 2, "0222112222120000")]


d08_input_img_size = (25, 6)


class Image:

    def __init__(self, width, height):
        self.width = width
        self.height = height
        self.layers = []
        self.rendered_layer = []

    def image_size(self):
        return self.width * self.height

    def fill_layers(self, str_input):
        layer_size = self.image_size()
        curr_layer = None
        for curr_digit in str_input:
            if curr_digit == '\n':
                print("Debug, found EndOfLine (\\n), stopping")
                break
            if curr_layer is None or len(curr_layer) >= layer_size:
                curr_layer = []
                self.layers.append(curr_layer)
            curr_layer.append(int(curr_digit))

    def print_all_layers(self):
        idx_layer_from_1 = 1
        for it_layer in self.layers:
            print("Layer", idx_layer_from_1, ":")
            idx_layer_from_1 += 1
            self.print_one_layer(it_layer)

    def print_rendered_layer(self):
        print("Rendered layer")
        self.print_one_layer(self.rendered_layer)

    def print_one_layer(self, layer):
        str_line = ""
        for curr_digit in layer:
            str_line += str(curr_digit)
            if len(str_line) >= self.width:
                print("        ", str_line)
                str_line = ""

    def render_image(self):
        # 0 is black, 1 is white, and 2 is transparent.
        all_pixels_rendered = {}  # dict of pixel indexes => color
        all_pixels_remaining = {x for x in range(0, self.image_size())}  # set of pixel indexes
        for it_layer in self.layers:
            # print("    checking new layer... ")
            # print("    all_pixels_rendered = ", all_pixels_rendered)
            # print("    all_pixels_remaining = ", all_pixels_remaining)
            for pixel_idx in all_pixels_remaining.copy():  # work on tmp copy since we remove elements during iteration
                if it_layer[pixel_idx] != 2:
                    all_pixels_remaining.remove(pixel_idx)
                    all_pixels_rendered[pixel_idx] = it_layer[pixel_idx]
        # print("    finished all layers... ")
        # print("    all_pixels_rendered = ", all_pixels_rendered)
        # print("    all_pixels_remaining = ", all_pixels_remaining)
        self.rendered_layer = []
        for x in range(0, self.image_size()):
            if x in all_pixels_rendered:
                self.rendered_layer.append(all_pixels_rendered[x])
            else:
                self.rendered_layer.append(2)


def do_tests_part1():
    print("==== do_tests_part1 ====")
    for it_test_img in d08_test_part1:
        width, height = it_test_img[0], it_test_img[1]
        str_raw_img = it_test_img[2]
        print("    ==== new image ==== : ", str_raw_img)
        img = Image(width, height)
        img.fill_layers(str_raw_img)
        img.print_all_layers()


def do_tests_part2():
    print("==== do_tests_part2 ====")
    for it_test_img in d08_test_part2:
        width, height = it_test_img[0], it_test_img[1]
        str_raw_img = it_test_img[2]
        print("    ==== new image ==== : ", str_raw_img)
        img = Image(width, height)
        img.fill_layers(str_raw_img)
        img.print_all_layers()
        img.render_image()
        img.print_rendered_layer()
        # ==== printed message looks like : "CGEGE" ====


def do_real_calculation_part1_and_2():
    print("==== do_real_calculation_part1_and_2 ====")
    with open("input.txt") as file: # Use file to refer to the file object
        str_input = file.readline()
        width, height = d08_input_img_size[0], d08_input_img_size[1]
        img = Image(width, height)
        img.fill_layers(str_input)
        # img.print_all_layers()
        min_digits_0 = None
        idx_layer_min_digits_0 = None
        for layer_idx in range(0, len(img.layers)):
            layer = img.layers[layer_idx]
            count_digits_0 = layer.count(0)
            if min_digits_0 is None or count_digits_0 < min_digits_0:
                min_digits_0 = count_digits_0
                idx_layer_min_digits_0 = layer_idx
        print("Layer idx ", idx_layer_min_digits_0, " (from 0) has the min number of digits 0 : ", min_digits_0)
        selected_layer = img.layers[idx_layer_min_digits_0]
        count_digits_1, count_digits_2 = selected_layer.count(1), selected_layer.count(2)
        print("On this layer : count(1) * count(2) = ", count_digits_1, " * ", count_digits_2, " = ", count_digits_1 * count_digits_2)
        img.render_image()
        img.print_rendered_layer()


def main():
    do_tests_part1()
    do_tests_part2()
    do_real_calculation_part1_and_2()


if __name__ == "__main__":
    # execute only if run as a script
    main()
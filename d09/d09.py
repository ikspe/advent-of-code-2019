
from d05.d05 import IntCodeComputer

d09_tests01 = ["109,2000,109,19,204,-34,99"]  # return what is in memory at position 1985
d09_tests02 = ["109,1,204,-1,1001,100,1,100,1008,100,16,101,1006,101,0,99"]  # takes no input and produces a copy of itself as output.
d09_tests03 = ["1102,34915192,34915192,7,4,7,99,0"]  # should output a 16-digit number.
d09_tests04 = ["104,1125899906842624,99"]  # should output the large number in the middle.


def do_tests_part1():
    print("==== do_tests_part1() ====")
    for it_test in d09_tests01:
        str_program = it_test
        print("    ==== d09_tests01 : str_program = ", str_program)
        for test_value in [0, 1, 2, 4, 42, 1337, -1987]:
            input_values = []
            output_values = []
            expected_output_values = [test_value]
            my_computer = IntCodeComputer()
            my_computer.create_program(str_program, input_values, output_values)
            my_computer.program.extend([0 for _ in range(len(my_computer.program), 1985 + 1)])
            my_computer.program[1985] = test_value
            if not my_computer.run_computer():
                print("ERROR FATAL. run_computer() FAILED !")
                return
            computed_output = my_computer.output_values
            if computed_output == expected_output_values:
                print("    SUCCESS of program for test_value ", test_value, " : computed_output = ", computed_output)
            else:
                print("    FAILURE of program for test_value ", test_value, " : computed_output = ", computed_output)
    for it_test in d09_tests02:
        str_program = it_test
        print("    ==== d09_tests02 : str_program = ", str_program)
        input_values = []
        output_values = []
        my_computer = IntCodeComputer(is_increase_memory=True)
        my_computer.create_program(str_program, input_values, output_values)
        expected_output_values = my_computer.program.copy()
        if not my_computer.run_computer():
            print("ERROR FATAL. run_computer() FAILED !")
            return
        computed_output = my_computer.output_values
        if computed_output == expected_output_values:
            print("    SUCCESS of program : computed_output = ", computed_output)
        else:
            print("    FAILURE of program : computed_output = ", computed_output)
    for it_test in d09_tests03:
        str_program = it_test
        print("    ==== d09_tests03 : str_program = ", str_program)
        input_values = []
        output_values = []
        my_computer = IntCodeComputer(is_increase_memory=True)
        my_computer.create_program(str_program, input_values, output_values)
        if not my_computer.run_computer():
            print("ERROR FATAL. run_computer() FAILED !")
            return
        computed_output = my_computer.output_values
        if len(computed_output) == 1 and len(str(computed_output[0])) == 16:
            print("    SUCCESS of program : computed_output = ", computed_output)
        else:
            print("    FAILURE of program : computed_output = ", computed_output)
    for it_test in d09_tests04:
        str_program = it_test
        print("    ==== d09_tests04 : str_program = ", str_program)
        input_values = []
        output_values = []
        my_computer = IntCodeComputer(is_increase_memory=True)
        my_computer.create_program(str_program, input_values, output_values)
        expected_output_values = [my_computer.program[1]]
        if not my_computer.run_computer():
            print("ERROR FATAL. run_computer() FAILED !")
            return
        computed_output = my_computer.output_values
        if computed_output == expected_output_values:
            print("    SUCCESS of program : computed_output = ", computed_output)
        else:
            print("    FAILURE of program : computed_output = ", computed_output)


def do_real_calculation_common(input_value):
    with open("input.txt") as file: # Use file to refer to the file object
        str_program = file.readline()
        input_values = [input_value]
        output_values = []
        my_computer = IntCodeComputer(is_increase_memory=True)
        my_computer.create_program(str_program, input_values, output_values)
        # my_computer.is_verbose = True
        if not my_computer.run_computer():
            print("ERROR FATAL. run_computer() FAILED !")
            return False
        print("    computer.is_finished = ", my_computer.is_finished)
        return my_computer.output_values


def do_real_calculation_part1():
    print("==== do_real_calculation_part1 ====")
    expected_output = [3780860499]  # we know this is the expected result... as we have already passed it
    computed_output = do_real_calculation_common(1)
    if computed_output == expected_output:
        print("SUCCESS of program : computed_output = ", computed_output, " same as expected_output")
    else:
        print("ERROR : success of program : but incorrect computed_output = ", computed_output, " : expected_output = ", expected_output)


def do_real_calculation_part2():
    print("==== do_real_calculation_part2 ====")
    expected_output = [33343]  # we know this is the expected result... as we have already passed it
    computed_output = do_real_calculation_common(2)
    print("SUCCESS of program : computed_output = ", computed_output)
    if computed_output == expected_output:
        print("SUCCESS of program : computed_output = ", computed_output, " same as expected_output")
    else:
        print("ERROR : success of program : but incorrect computed_output = ", computed_output, " : expected_output = ", expected_output)


def main():
    do_tests_part1()
    do_real_calculation_part1()
    # do_tests_part2()
    do_real_calculation_part2()


if __name__ == "__main__":
    # execute only if run as a script
    main()
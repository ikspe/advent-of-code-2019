import math

moons_test01_01 = [["<x=-1, y=0, z=2>",
                    "<x=2, y=-10, z=-7>",
                    "<x=4, y=-8, z=8>",
                    "<x=3, y=5, z=-1>"], 1, 10, 179, 2772]  # print every 1 steps, 10 steps , expected energy = 179, repetition =2772
moons_test01_02 = [["<x=-8, y=-10, z=0>",
                     "<x=5, y=5, z=10>",
                     "<x=2, y=-7, z=3>",
                     "<x=9, y=-8, z=-3>"], 10, 100, 1940, 4686774924]  # print every 10 steps, 100 steps, expected energy = 1940 , repetition = 4686774924

moons_test01_all = [moons_test01_01, moons_test01_02]


class Moon:

    def __init__(self):
        self.pos = {'x': 0, 'y': 0, 'z': 0}
        self.vel = {'x': 0, 'y': 0, 'z': 0}

    def set_position_from_str(self, str_input):
        """str_input like : <x=-1, y=0, z=2>"""
        str_input_maps = {'pos': '', 'vel': ''}
        dict_input_maps = dict()
        idx_pos = str_input.find("pos")
        if idx_pos == -1:
            str_input_maps['pos'] = str_input
        else:
            idx_pos_equals = str_input.find("=", idx_pos)
            idx_vel = str_input.find("vel", idx_pos_equals)
            idx_vel_equals = str_input("=", idx_vel)
            str_input_maps['pos'] = str_input[idx_pos_equals+1:idx_vel]
            str_input_maps['vel'] = str_input[idx_vel_equals+1:]

        # print("str_input_maps = ", str_input_maps)

        for it_pos_or_vel, it_str in str_input_maps.items():
            if it_str == '':
                # print("it_pos_or_vel = ", it_pos_or_vel, ", it_str is empty : skipped")
                continue

            idx_begin = it_str.find('<')
            idx_end = it_str.find('>', idx_begin)

            # print("it_pos_or_vel = ", it_pos_or_vel, ", idx_begin = ", idx_begin, ", idx_end = ", idx_end)

            if idx_begin == -1 or idx_end == -1:
                print("ERROR, bad string input : ", str_input, " impossible to parse: ", it_pos_or_vel, "begin,end = ", idx_begin, idx_end)
                return False

            str_tmp = it_str[idx_begin+1:idx_end]
            # print("it_pos_or_vel = ", it_pos_or_vel, ", str_tmp = ", str_tmp)
            tokens = str_tmp.split(', ')
            tmp_coords = dict()
            for it_token in tokens:
                name, value = it_token.split('=')
                tmp_coords[name] = int(value)
            # print("str_input = ", str_input, " ; found it_pos_or_vel = ", it_pos_or_vel, ", tmp_coords = ", tmp_coords)
            dict_input_maps[it_pos_or_vel] = tmp_coords

        if 'pos' in dict_input_maps:
            for key, value in dict_input_maps['pos'].items():
                self.pos[key] = value
        if 'vel' in dict_input_maps:
            for key, value in dict_input_maps['vel'].items():
                self.vel[key] = value

        return True

    def get_moon_as_str(self):
        out_str = ''
        list_keys_coords = ['x', 'y', 'z']
        dict_to_print = {'pos': self.pos, 'vel': self.vel}
        for pos_or_vel, coords in dict_to_print.items():
            out_str += pos_or_vel + "=<"
            first_key_coord = True
            for key_coord in list_keys_coords:
                if not first_key_coord:
                    out_str += ", "
                first_key_coord = False
                out_str += key_coord + "={:4d}".format(coords[key_coord])
            out_str += ">"
            out_str += ", "
        return out_str

    def print_moon(self):
        print(self.get_moon_as_str())

    def is_equals(self, other):
        if self.pos != other.pos:
            return False
        if self.vel != other.vel:
            return False
        return True

    def is_equals_on_axis(self, other, axis):
        if self.pos[axis] != other.pos[axis]:
            return False
        if self.vel[axis] != other.vel[axis]:
            return False
        return True

    def apply_velocity(self):
        for key in self.pos.keys():
            self.pos[key] += self.vel[key]

    def apply_gravity(self, vel_changes):
        # print("moon : ", self.get_moon_as_str(), " apply_velocity_changes_from_interactions() with input = ", vel_changes)
        for key in self.vel.keys():
            self.vel[key] += vel_changes[key]

    def get_potential_energy(self):
        energy = 0
        for val in self.pos.values():
            energy += abs(val)
        return energy

    def get_kinetic_energy(self):
        energy = 0
        for val in self.vel.values():
            energy += abs(val)
        return energy

    def get_total_energy(self):
        return self.get_potential_energy() * self.get_kinetic_energy()

    pass


def compute_vel_from_gravity(list_moons, idx_moon):
    list_keys_coords = ['x', 'y', 'z']
    vel_changes = {x: 0 for x in list_keys_coords}
    curr_moon = list_moons[idx_moon]

    # print("moon at idx ", idx_moon, " : ", curr_moon.get_moon_as_str(), " current vel_changes = ", vel_changes)

    for idx_other in range(0, len(list_moons)):
        if idx_other == idx_moon:
            continue  # no changes on itself... Even if doesn't matter
        other_moon = list_moons[idx_other]
        for key_coord in list_keys_coords:
            diff_pos = (curr_moon.pos[key_coord] - other_moon.pos[key_coord])
            if diff_pos > 0:
                vel_changes[key_coord] -= 1
            elif diff_pos < 0:
                vel_changes[key_coord] += 1
    return vel_changes


def find_least_common_multiple(list_numbers):
    current_lcm = 1
    for num in list_numbers:
        print("current_lcm = ", current_lcm, " , num = ", num)
        gcd = math.gcd(current_lcm, num)
        current_lcm = current_lcm * num // gcd
    print("final current_lcm = ", current_lcm)
    return current_lcm


def do_tests_part1():
    print("==== do_tests_part1() ====")
    for it_test_moons in moons_test01_all:

        str_moons = it_test_moons[0]
        print_every_x_steps = it_test_moons[1]
        nb_steps_to_run = it_test_moons[2]
        expected_total_energy = it_test_moons[3]

        list_moons = []
        for it_str in str_moons:
            my_moon = Moon()
            my_moon.set_position_from_str(it_str)
            list_moons.append(my_moon)

        for current_step in range(0, nb_steps_to_run + 1):

            if current_step % print_every_x_steps == 0:
                print("After " + str(current_step) + " steps")

                for it_moon in list_moons:
                    it_moon.print_moon()

            if current_step >= nb_steps_to_run:
                break

            list_vel_changes = [{} for x in list_moons]
            for idx in range(0, len(list_moons)):
                list_vel_changes[idx] = compute_vel_from_gravity(list_moons, idx)
                # print("gravity : moon = ", list_moons[idx].get_moon_as_str(), " , vel_changes = ", list_vel_changes[idx])

            for idx in range(0, len(list_moons)):
                curr_moon = list_moons[idx]
                curr_moon.apply_gravity(list_vel_changes[idx])
                curr_moon.apply_velocity()

        total_system_energy = 0
        for it_moon in list_moons:
            moon_energy = it_moon.get_total_energy()
            print("moon energy = ", moon_energy)
            total_system_energy += moon_energy

        if expected_total_energy == total_system_energy:
            print("SUCCESS : total system energy = ", total_system_energy, " , expected energy = ", expected_total_energy)
        else:
            print("FAILURE : total system energy = ", total_system_energy, " , expected energy = ", expected_total_energy)


def calculation_part2(str_moons):
    list_axis_remaining_to_find = ['x', 'y', 'z']

    found_repetition_axis = {}
    nb_steps_to_run_max = 500000
    print_every_x_steps = 50000

    list_moons = []
    list_moons_beginning = []
    for it_str in str_moons:
        my_moon = Moon()
        my_moon.set_position_from_str(it_str)
        list_moons.append(my_moon)
        my_moon_beginning = Moon()
        my_moon_beginning.set_position_from_str(it_str)
        list_moons_beginning.append(my_moon_beginning)

    for current_step in range(0, nb_steps_to_run_max + 1):

        if current_step % print_every_x_steps == 0:
            print("After " + str(current_step) + " steps")

            for it_moon in list_moons:
                it_moon.print_moon()

        if current_step >= nb_steps_to_run_max:
            break

        list_vel_changes = [{} for x in list_moons]
        for idx in range(0, len(list_moons)):
            list_vel_changes[idx] = compute_vel_from_gravity(list_moons, idx)
            # print("gravity : moon = ", list_moons[idx].get_moon_as_str(), " , vel_changes = ", list_vel_changes[idx])

        for idx in range(0, len(list_moons)):
            curr_moon = list_moons[idx]
            curr_moon.apply_gravity(list_vel_changes[idx])
            curr_moon.apply_velocity()

        at_least_one_axis_repetition_found = False
        for axis in list_axis_remaining_to_find.copy():
            all_equals = True
            for idx in range(0, len(list_moons)):
                curr_moon = list_moons[idx]
                moon_beginning = list_moons_beginning[idx]
                # print("Testing idx = ", idx, " : moon = ", curr_moon.get_moon_as_str(), " , moonBeginning = ", moon_beginning.get_moon_as_str())
                if not curr_moon.is_equals_on_axis(moon_beginning, axis):
                    all_equals = False
                    break
            if all_equals:
                at_least_one_axis_repetition_found = True
                print("Found repetition for axis ", axis, " at iteration = ", current_step + 1)
                found_repetition_axis[axis] = current_step + 1
                list_axis_remaining_to_find.remove(axis)
        if at_least_one_axis_repetition_found:
            if len(list_axis_remaining_to_find) == 0:
                print("All axis repetition found at iteration : ", current_step + 1, ", stopping")
                break

    print("list_axis_remaining_to_find = ", list_axis_remaining_to_find)
    print("found_repetition_axis = ", found_repetition_axis)
    repetition_lcm = find_least_common_multiple(found_repetition_axis.values())
    print("repetition lcm = ", repetition_lcm)
    return repetition_lcm


def do_tests_part2():
    print("==== do_tests_part2() ====")
    for it_test_moons in moons_test01_all:
        str_moons = it_test_moons[0]
        expected_repetition = it_test_moons[4]
        repetition_lcm_computed = calculation_part2(str_moons)

        if expected_repetition == repetition_lcm_computed:
            print("SUCCESS : repetition lcm computed = ", repetition_lcm_computed, " , expected repetition = ", expected_repetition)
        else:
            print("FAILURE : repetition lcm computed = ", repetition_lcm_computed, " ,expected repetition = ", expected_repetition)


def do_real_calculation_part1():
    print("==== do_real_calculation_part1() ====")
    str_moons = []
    with open("input.txt") as file: # Use file to refer to the file object
        line = file.readline()
        while line:
            str_moons.append(line)
            line = file.readline()

    print_every_x_steps = 200
    nb_steps_to_run = 1000
    expected_total_energy = 9999  # i know this is the good reply, to test non-regression

    list_moons = []
    for it_str in str_moons:
        my_moon = Moon()
        my_moon.set_position_from_str(it_str)
        list_moons.append(my_moon)

    for current_step in range(0, nb_steps_to_run + 1):

        if current_step % print_every_x_steps == 0:
            print("After " + str(current_step) + " steps")

            for it_moon in list_moons:
                it_moon.print_moon()

        if current_step >= nb_steps_to_run:
            break

        list_vel_changes = [{} for x in list_moons]
        for idx in range(0, len(list_moons)):
            list_vel_changes[idx] = compute_vel_from_gravity(list_moons, idx)
            # print("gravity : moon = ", list_moons[idx].get_moon_as_str(), " , vel_changes = ", list_vel_changes[idx])

        for idx in range(0, len(list_moons)):
            curr_moon = list_moons[idx]
            curr_moon.apply_gravity(list_vel_changes[idx])
            curr_moon.apply_velocity()

    total_system_energy = 0
    for it_moon in list_moons:
        moon_energy = it_moon.get_total_energy()
        print("moon energy = ", moon_energy)
        total_system_energy += moon_energy

    if expected_total_energy == total_system_energy:
        print("SUCCESS : total system energy = ", total_system_energy, " , expected energy = ", expected_total_energy)
    else:
        print("FAILURE : total system energy = ", total_system_energy, " , expected energy = ", expected_total_energy)


def do_real_calculation_part2():
    print("==== do_real_calculation_part2() ====")
    str_moons = []
    with open("input.txt") as file: # Use file to refer to the file object
        line = file.readline()
        while line:
            str_moons.append(line)
            line = file.readline()

    expected_repetition = 282399002133976  # I have computed this is the correct answer : check it in non-regressions
    repetition_lcm_computed = calculation_part2(str_moons)

    if expected_repetition == repetition_lcm_computed:
        print("SUCCESS : repetition lcm computed = ", repetition_lcm_computed, " , expected repetition = ", expected_repetition)
    else:
        print("FAILURE : repetition lcm computed = ", repetition_lcm_computed, " ,expected repetition = ", expected_repetition)


def main():
    do_tests_part1()
    do_real_calculation_part1()
    do_tests_part2()
    do_real_calculation_part2()


if __name__ == "__main__":
    # execute only if run as a script
    main()
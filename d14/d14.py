import math

test_01 = [
    [["10 ORE => 10 A",
      "1 ORE => 1 B",
      "7 A, 1 B => 1 C",
      "7 A, 1 C => 1 D",
      "7 A, 1 D => 1 E",
      "7 A, 1 E => 1 FUEL"], 31],  # part1 expected ORE : 31
    [["9 ORE => 2 A",
      "8 ORE => 3 B",
      "7 ORE => 5 C",
      "3 A, 4 B => 1 AB",
      "5 B, 7 C => 1 BC",
      "4 C, 1 A => 1 CA",
      "2 AB, 3 BC, 4 CA => 1 FUEL"], 165],  # part1 expected ORE : 165
    [["157 ORE => 5 NZVS",
      "165 ORE => 6 DCFZ",
      "44 XJWVT, 5 KHKGT, 1 QDVJ, 29 NZVS, 9 GPVTF, 48 HKGWZ => 1 FUEL",
      "12 HKGWZ, 1 GPVTF, 8 PSHF => 9 QDVJ",
      "179 ORE => 7 PSHF",
      "177 ORE => 5 HKGWZ",
      "7 DCFZ, 7 PSHF => 2 XJWVT",
      "165 ORE => 2 GPVTF",
      "3 DCFZ, 7 NZVS, 5 HKGWZ, 10 PSHF => 8 KHKGT"], 13312, 82892753],  # part1 expected ORE : 13312 ; part2 with one trillion ORE : produce 82892753 FUEL
    [["2 VPVL, 7 FWMGM, 2 CXFTF, 11 MNCFX => 1 STKFG",
      "17 NVRVD, 3 JNWZP => 8 VPVL",
      "53 STKFG, 6 MNCFX, 46 VJHF, 81 HVMC, 68 CXFTF, 25 GNMV => 1 FUEL",
      "22 VJHF, 37 MNCFX => 5 FWMGM",
      "139 ORE => 4 NVRVD",
      "144 ORE => 7 JNWZP",
      "5 MNCFX, 7 RFSQX, 2 FWMGM, 2 VPVL, 19 CXFTF => 3 HVMC",
      "5 VJHF, 7 MNCFX, 9 VPVL, 37 CXFTF => 6 GNMV",
      "145 ORE => 6 MNCFX",
      "1 NVRVD => 8 CXFTF",
      "1 VJHF, 6 MNCFX => 4 RFSQX",
      "176 ORE => 6 VJHF"], 180697, 5586022],  # part1 expected ORE : 180697 ; part2 with one trillion ORE : produce 5586022 FUEL
    [["171 ORE => 8 CNZTR",
      "7 ZLQW, 3 BMBT, 9 XCVML, 26 XMNCP, 1 WPTQ, 2 MZWV, 1 RJRHP => 4 PLWSL",
      "114 ORE => 4 BHXH",
      "14 VRPVC => 6 BMBT",
      "6 BHXH, 18 KTJDG, 12 WPTQ, 7 PLWSL, 31 FHTLT, 37 ZDVW => 1 FUEL",
      "6 WPTQ, 2 BMBT, 8 ZLQW, 18 KTJDG, 1 XMNCP, 6 MZWV, 1 RJRHP => 6 FHTLT",
      "15 XDBXC, 2 LTCX, 1 VRPVC => 6 ZLQW",
      "13 WPTQ, 10 LTCX, 3 RJRHP, 14 XMNCP, 2 MZWV, 1 ZLQW => 1 ZDVW",
      "5 BMBT => 4 WPTQ",
      "189 ORE => 9 KTJDG",
      "1 MZWV, 17 XDBXC, 3 XCVML => 2 XMNCP",
      "12 VRPVC, 27 CNZTR => 2 XDBXC",
      "15 KTJDG, 12 BHXH => 5 XCVML",
      "3 BHXH, 2 VRPVC => 7 MZWV",
      "121 ORE => 7 VRPVC",
      "7 XCVML => 6 RJRHP",
      "5 BHXH, 4 VRPVC => 5 LTCX"], 2210736, 460664],  # part1 expected ORE : 2210736 ; part2 with one trillion ORE : produce 460664 FUEL
]

is_verbose_reaction_from_str = False


class Reaction:

    def __init__(self):
        self.inputs = dict()  # key = product ; value = quantity
        self.outputs = dict()  # key = product ; value = quantity
        self.reaction_ancestors = []

    def set_reaction_from_str(self, reaction_str):
        global is_verbose_reaction_from_str
        tokens = reaction_str.split('=>')
        if is_verbose_reaction_from_str:
            print("Debug : reaction_str = ", reaction_str, "    =====> tokens = ", tokens)
        if len(tokens) != 2:
            print("Error : invalid reaction : not finding 2 tokens : ", reaction_str)
            return False
        [inputs_str, outputs_str] = tokens[0:2]
        self.inputs = self.parse_products_from_str(inputs_str)
        self.outputs = self.parse_products_from_str(outputs_str)
        if (self.inputs is False) or (self.outputs is False):
            print("Error, impossible to parse inputs_str or output_str : {", inputs_str, outputs_str,
                  "} ; interpreted = {", self.inputs, self.outputs, "}")
            return False
        return True

    @staticmethod
    def parse_products_from_str(str_to_parse):
        global is_verbose_reaction_from_str
        tokens = str_to_parse.split(',')
        products_from_str = {}
        for one_product_str in tokens:
            tokens_one_product = one_product_str.split(' ')
            if is_verbose_reaction_from_str:
                print("Debug : one_product_str = ", one_product_str, "    =====> tokens = ", tokens_one_product)
            while True:
                try:
                    tokens_one_product.remove('')
                except ValueError:
                    break  #   no empty token anymore in list
            if is_verbose_reaction_from_str:
                print("Debug : one_product_str = ", one_product_str, "    =====> tokens = ", tokens_one_product)
            if len(tokens_one_product) != 2:
                print("Error : invalid product : not finding 2 tokens : ", tokens_one_product)
                return False
            product, quantity = tokens_one_product[1], int(tokens_one_product[0])
            products_from_str[product] = quantity
        return products_from_str

    def init_compute_ancestors(self, reaction_list):
        global is_verbose_reaction_from_str
        if is_verbose_reaction_from_str:
            print("Debug. init_compute_ancestors(). self = ", self.get_reaction_as_str())
        remaining_reaction_list = reaction_list.copy()
        remaining_reaction_list.remove(self)
        list_inputs_to_find = [x for x in self.inputs.keys()]
        while len(list_inputs_to_find) > 0:
            input_to_find = list_inputs_to_find[0]
            # print("Debug. init_compute_ancestors(). list_inputs_to_find = ", list_inputs_to_find, " , input_to_find = ", input_to_find)
            for reaction in remaining_reaction_list.copy():
                if input_to_find in reaction.outputs:
                    if is_verbose_reaction_from_str:
                        print("Debug. Found ancestor : reaction = ", reaction.get_reaction_as_str())
                    self.reaction_ancestors.append(reaction)
                    remaining_reaction_list.remove(reaction)
                    # print("Debug. Adding ancestor inputs = ", [x for x in reaction.inputs.keys()])
                    list_inputs_to_find.extend([x for x in reaction.inputs.keys()])
            list_inputs_to_find.remove(input_to_find)

    def has_this_reaction_as_ancestor(self, other):
        if other in self.reaction_ancestors:
            return True
        return False

    def get_reaction_as_str(self):
        return str(self.inputs) + " => " + str(self.outputs)

    pass  # class Reaction


class NanoFactory:

    def __init__(self):
        self.reactions = []

    def set_reaction_from_str_list(self, reaction_str_list):
        global is_verbose_reaction_from_str
        for reaction_str in reaction_str_list:
            reaction = Reaction()
            if not reaction.set_reaction_from_str(reaction_str):
                print("ERROR interpreting reaction : ", reaction_str)
                return False
            if is_verbose_reaction_from_str:
                print("Debug : correctly interpreted reaction from string : ", reaction_str, " =====> inputs = ",
                      reaction.inputs, " , outputs = ", reaction.outputs)
            self.reactions.append(reaction)
        for reaction in self.reactions:
            reaction.init_compute_ancestors(self.reactions)
        return True

    def is_product_1_ancestor_of_2(self, product1, product2):
        for reaction in self.reactions:
            if product2 in reaction.outputs:
                if product1 in reaction.inputs:
                    return True
                for react_ancestor in reaction.reaction_ancestors:
                    if product1 in react_ancestor.inputs:
                        return True
        return False

    def find_inputs_to_get_one_fuel(self):
        current_products = {"FUEL": 1}
        return self.find_required_ore_for_products(current_products.copy())

    def find_required_ore_for_products(self, current_products):
        unused_intermediate = {}
        stop_when_only_product = "ORE"
        while True:
            if len(current_products) == 0:
                print("ERROR : nothing left")
                return False
            current_products_keys = [x for x in current_products.keys()]
            if current_products_keys == [stop_when_only_product]:
                # print("Found : current_products = ", current_products)
                break

            product_to_synthesize = current_products_keys[0]
            while True:
                finished_finding_product = True
                for it_product in current_products.keys():
                    if self.is_product_1_ancestor_of_2(product_to_synthesize, it_product):
                        product_to_synthesize = it_product
                        finished_finding_product = False
                        break
                if finished_finding_product:
                    break
            quantity = current_products[product_to_synthesize]
            # print("product_to_synthesize = ", product_to_synthesize)

            del current_products[product_to_synthesize]
            # print("Debug. Input to find for product to synthesize = ", product_to_synthesize, " , quantity = ", quantity)
            input_found = False
            for reaction in self.reactions:
                # print("Debug. Checking reaction. output = ", reaction.outputs)
                if (product_to_synthesize in reaction.outputs) and (len(reaction.outputs) == 1):
                    input_found = True
                    # print("Debug. Input found : ", reaction.get_reaction_as_str())
                    nb_reactions_to_run = math.ceil(quantity / reaction.outputs[product_to_synthesize])
                    unused_quantity = reaction.outputs[product_to_synthesize] * nb_reactions_to_run - quantity
                    if unused_quantity != 0:
                        # print("Debug. Unused quantity of ", product_to_synthesize, " : ", unused_quantity)
                        if product_to_synthesize not in unused_intermediate:
                            unused_intermediate[product_to_synthesize] = 0
                        unused_intermediate[product_to_synthesize] += unused_quantity
                    # print("Debug. Input found : ", reaction.get_reaction_as_str(), " : number reactions to run : ", nb_reactions_to_run)
                    for it_input, quantity_input in reaction.inputs.items():
                        if it_input not in current_products:
                            current_products[it_input] = 0
                        current_products[it_input] += (nb_reactions_to_run * quantity_input)
                    break

        return current_products, unused_intermediate

    pass  # class NanoFactory


def common_part1(str_list_reactions, expected_quantity):
    factory = NanoFactory()
    if not factory.set_reaction_from_str_list(str_list_reactions):
        print("ERROR in set_reaction_from_str_list() from reaction_str_list = ", str_list_reactions)
        return False
    needed_products_one_fuel, unused_intermediate = factory.find_inputs_to_get_one_fuel()
    if "ORE" not in needed_products_one_fuel:
        print("ERROR, not found ORE ; needed products : ", needed_products_one_fuel)
        return False
    if len(needed_products_one_fuel) > 1:
        print("ERROR, too many needed products : ", needed_products_one_fuel)
        return False
    needed_quantity = needed_products_one_fuel["ORE"]
    if needed_quantity == expected_quantity:
        print("SUCCESS : expected ", expected_quantity, " , found = ", needed_quantity, " , unused_intermediate = ", unused_intermediate)
    else:
        print("FAILURE : expected ", expected_quantity, " , found = ", needed_quantity, " , unused_intermediate = ", unused_intermediate)
        return False
    return needed_quantity


def do_tests_part1():
    print("==== do_tests_part1() ====")
    return_ok = True
    for it_test in test_01:
        str_list_reactions = it_test[0]
        expected_quantity = it_test[1]
        if common_part1(str_list_reactions, expected_quantity) is False:
            return_ok = False
    if not return_ok:
        print("FAILURE of do_tests_part1()")


def do_real_calculation_part1():
    print("==== do_real_calculation_part1() ====")
    str_list_reactions = []
    with open("input.txt") as file:  # Use file to refer to the file object
        line = file.readline()
        while line:
            # print("line read : ", line, " ==========")
            str_list_reactions.append(line.strip())
            line = file.readline()

    expected_quantity = 136771  # I know this is the good answer : for non-regression tests
    result_ore = common_part1(str_list_reactions, expected_quantity)
    if result_ore is False:
        print("FAILURE of do_real_calculation_part1()")
    else:
        print("do_real_calculation_part1() : result_ore = ", result_ore)


def common_part2(str_list_reactions, expected_fuel_for_trillion):
    factory = NanoFactory()
    if not factory.set_reaction_from_str_list(str_list_reactions):
        print("ERROR in set_reaction_from_str_list() from reaction_str_list = ", str_list_reactions)
        return False
    needed_products, unused_intermediate = factory.find_inputs_to_get_one_fuel()
    if "ORE" not in needed_products:
        print("ERROR, not found ORE ; needed products : ", needed_products)
        return False
    if len(needed_products) > 1:
        print("ERROR, too many needed products : ", needed_products)
        return False
    needed_quantity_ore_one_fuel = needed_products["ORE"]
    print("needed_quantity_ore = ", needed_quantity_ore_one_fuel, " , unused_intermediate = ", unused_intermediate)

    trillion = 1000000000000
    basic_fuel_equivalent_for_trillion_ore = trillion // needed_quantity_ore_one_fuel

    # unused_intermediate_for_basic_fuel_trillion_ore = {x: y * basic_fuel_equivalent_for_trillion_ore for x, y in unused_intermediate.items()}
    print("basic_fuel_equivalent_for_trillion_ore = ", basic_fuel_equivalent_for_trillion_ore)
    # print("unused_intermediate_for_basic_fuel_trillione_ore = ", unused_intermediate_for_basic_fuel_trillion_ore)

    current_fuel_estimation = basic_fuel_equivalent_for_trillion_ore
    while True:
        current_needed_products, current_unused_intermediate = factory.find_required_ore_for_products({"FUEL": current_fuel_estimation})
        current_needed_quantity_ore_one_fuel = current_needed_products["ORE"]
        # print("current_fuel_estimation = ", current_fuel_estimation)
        # print("current_needed_products = ", current_needed_products)
        # print("current_needed_quantity_ore_one_fuel = ", current_needed_quantity_ore_one_fuel)
        # print("current_unused_intermediate = ", current_unused_intermediate)
        if current_needed_quantity_ore_one_fuel > trillion:
            print("ERROR. current_needed_quantity_ore_one_fuel > trillion")
            return False
        diff_supposed_fuel = (trillion - current_needed_quantity_ore_one_fuel) // needed_quantity_ore_one_fuel
        if diff_supposed_fuel == 0:
            # print("diff_supposed_fuel == 0    => stopping")
            break
        # print("diff_supposed_fuel = ", diff_supposed_fuel, " => shall increment current_fuel_estimation... (TODO)")
        current_fuel_estimation += diff_supposed_fuel

    print("FINAL : current_fuel_estimation = ", current_fuel_estimation)
    print("")

    if current_fuel_estimation == expected_fuel_for_trillion:
        print("SUCCESS : expected ", expected_fuel_for_trillion, " , found = ", current_fuel_estimation)
    else:
        print("FAILURE : expected ", expected_fuel_for_trillion, " , found = ", current_fuel_estimation)
        return False
    return current_fuel_estimation


def do_tests_part2():
    print("==== do_tests_part2() ====")
    return_ok = True
    for it_test in test_01:
        str_list_reactions = it_test[0]
        if len(it_test) < 3:
            print("Debug. Skipping this test : no expected result")
            continue
        expected_fuel_for_trillion = it_test[2]
        if common_part2(str_list_reactions, expected_fuel_for_trillion) is False:
            return_ok = False
    if not return_ok:
        print("FAILURE of do_tests_part2()")


def do_real_calculation_part2():
    print("==== do_real_calculation_part2() ====")
    str_list_reactions = []
    with open("input.txt") as file:  # Use file to refer to the file object
        line = file.readline()
        while line:
            # print("line read : ", line, " ==========")
            str_list_reactions.append(line.strip())
            line = file.readline()

    expected_quantity = 8193614  # I know this is the good answer : for non-regression tests
    result_ore = common_part2(str_list_reactions, expected_quantity)
    if result_ore is False:
        print("FAILURE of do_real_calculation_part2()")
    else:
        print("do_real_calculation_part2() : result_ore = ", result_ore)


def main():
    global is_verbose_reaction_from_str
    do_tests_part1()
    # is_verbose_reaction_from_str = True
    do_real_calculation_part1()
    do_tests_part2()
    do_real_calculation_part2()


if __name__ == "__main__":
    # execute only if run as a script
    main()


from d05.d05 import IntCodeComputer


test_val_part1 = [
                '#.........',
                '.#........',
                '..##......',
                '...###....',
                '....###...',
                '.....####.',
                '......####',
                '......####',
                '.......###',
                '........##', ]
test_val_part2 = [
                '#.......................................',
                '.#......................................',
                '..##....................................',
                '...###..................................',
                '....###.................................',
                '.....####...............................',
                '......#####.............................',
                '......######............................',
                '.......#######..........................',
                '........########........................',
                '.........#########......................',
                '..........#########.....................',
                '...........##########...................',
                '...........############.................',
                '............############................',
                '.............#############..............',
                '..............##############............',
                '...............###############..........',
                '................###############.........',
                '................#################.......',
                '.................########OOOOOOOOOO.....',
                '..................#######OOOOOOOOOO#....',
                '...................######OOOOOOOOOO###..',
                '....................#####OOOOOOOOOO#####',
                '.....................####OOOOOOOOOO#####',
                '.....................####OOOOOOOOOO#####',
                '......................###OOOOOOOOOO#####',
                '.......................##OOOOOOOOOO#####',
                '........................#OOOOOOOOOO#####',
                '.........................OOOOOOOOOO#####',
                '..........................##############',
                '..........................##############',
                '...........................#############',
                '............................############',
                '.............................###########', ]


def common_calculation_part1(max_x, max_y, quick_clever_computation=False):
    with open("input.txt") as file: # Use file to refer to the file object
        str_program = file.readline()
        affected_points = [['.' for x in range(0, max_x)] for x in range(0, max_y)]
        last_line_first_signal = 0
        last_line_last_signal = 0
        nb_computation_done = 0
        for y in range(0, max_y):
            this_line_has_found_first_signal = False
            this_line_last_signal = 0
            # print("computing line y = ", y, " ; last_line_first_signal = ", last_line_first_signal, " ; last_line_last_signal = ", last_line_last_signal)
            for x in range(0, max_x):
                if quick_clever_computation:
                    if x < last_line_first_signal:
                        # print("computing line y = ", y, " ; x = ", x, " => skipped (nothing)")
                        continue  # given the shape of the beam, no need to compute this value, we know there is nothing
                    if this_line_has_found_first_signal and x <= last_line_last_signal:
                        # print("computing line y = ", y, " ; x = ", x, " => skipped (something)")
                        affected_points[y][x] = '#'  # given the shape of the beam, no need to compute this value, we know there is something
                        continue
                input_values = [x, y]
                output_values = []
                nb_computation_done += 1
                my_computer = IntCodeComputer(is_increase_memory=True)
                my_computer.create_program(str_program, input_values, output_values)
                # my_computer.is_verbose = True
                if not my_computer.run_computer():
                    print("ERROR FATAL. run_computer() FAILED !")
                    return False
                # print("    (x, y) = (", x, y, ") ; computer.is_finished = ", my_computer.is_finished)
                out_value = '.'
                if my_computer.output_values[0] == 1:
                    out_value = '#'
                    if not this_line_has_found_first_signal:
                        last_line_first_signal = x
                    this_line_has_found_first_signal = True
                    this_line_last_signal = x
                else:
                    out_value = '.'
                affected_points[y][x] = out_value
                if quick_clever_computation:
                    if out_value == '.' and this_line_has_found_first_signal:
                        break
            last_line_last_signal = this_line_last_signal

        print("INFO : nb_computation_done = ", nb_computation_done, " on total x*y = ", x*y, " = ", nb_computation_done * 100 / (x*y), "%%")
        return affected_points


def display_affected_points(affected_points):
    for line_result in affected_points:
        str_line = ''.join(line_result)
        print(str_line)


def count_affected_points(affected_points):
    nb_affected_points = 0
    for line_result in affected_points:
        for val in line_result:
            if val == '#':
                nb_affected_points += 1
    return nb_affected_points


def do_tests_part1():
    print("==== do_tests_part1() ====")
    affected_points = []
    for line in test_val_part1:
        affected_points.append([c for c in line])
    display_affected_points(affected_points)
    nb_affected_points = count_affected_points(affected_points)
    expected_nb_affected_points = 27
    if nb_affected_points == expected_nb_affected_points:
        print("SUCCESS : correct value computed : nb_affected_points = ", nb_affected_points, " ; expected ", expected_nb_affected_points)
    else:
        print("FAILURE : wrong   value computed : nb_affected_points = ", nb_affected_points, " ; expected ", expected_nb_affected_points)


def do_real_calculation_part1():
    print("==== do_real_calculation_part1() ====")
    affected_points = common_calculation_part1(50, 50, quick_clever_computation=True)
    display_affected_points(affected_points)
    nb_affected_points = count_affected_points(affected_points)
    expected_nb_affected_points = 121  # found value of part1 is 121, kept for non-regression
    if nb_affected_points == expected_nb_affected_points:
        print("SUCCESS : correct value computed : nb_affected_points = ", nb_affected_points, " ; expected ", expected_nb_affected_points)
    else:
        print("FAILURE : wrong   value computed : nb_affected_points = ", nb_affected_points, " ; expected ", expected_nb_affected_points)

"""
def find_biggest_square_at_position(affected_points, x, y):
    max_square_size_found = 0
    width, height = len(affected_points[0]), len(affected_points)

    # print("find_biggest_square_at_position() starting with (x, y) = (", x, ", ", y, ")")
    while True:
        max_square_size_candidate = max_square_size_found + 1
        if x + max_square_size_candidate > width or y + max_square_size_candidate > height:
            # print("stopping with max_square_size_found = ", max_square_size_found, " : max_square_size_candidate = ", max_square_size_candidate, " ; x+max > width or y+max ")
            return max_square_size_found
        for it_y in range(0, max_square_size_candidate):
            for it_x in range(0, max_square_size_candidate):
                if affected_points[y + it_y][x + it_x] == '.':
                    # print("stopping with max_square_size_found = ", max_square_size_found, " : max_square_size_candidate = ", max_square_size_candidate,
                    #       " ; affected_points[y + it_y][x + it_x] = affected_points[y + ", it_y, "][x + ", it_x, "] = affected_points[", y + it_y, "][", x + it_x, "] = ", affected_points[y + it_y][x + it_x])
                    return max_square_size_found
        max_square_size_found = max_square_size_candidate
"""


def does_santa_fit_at_position(affected_points, x, y, santa_square_size):
    width, height = len(affected_points[0]), len(affected_points)

    if x + santa_square_size > width or y + santa_square_size > height:
        return False
    for it_y in range(0, santa_square_size):
        for it_x in range(0, santa_square_size):
            if affected_points[y + it_y][x + it_x] == '.':
                return False
    return True


def do_tests_part2():
    print("==== do_tests_part2() ====")
    affected_points = []
    for line in test_val_part2:
        affected_points.append(['#' if c == 'O' else c for c in line])
    santa_square_size = 10
    computed_result = common_calculation_part2(affected_points, santa_square_size)
    expected_result = 250020
    if computed_result == expected_result:
        print("SUCCESS : correct value computed : computed_result = ", computed_result, " ; expected ", expected_result)
    else:
        print("FAILURE : wrong   value computed : computed_result = ", computed_result, " ; expected ", expected_result)


def do_real_calculation_part2():
    print("==== do_real_calculation_part2() ====")
    affected_points = common_calculation_part1(2000, 1000, quick_clever_computation=True)
    santa_square_size = 100
    computed_result = common_calculation_part2(affected_points, santa_square_size)
    expected_result = 15090773  # I have computed it a first time, not it's to test non-regression
    if computed_result == expected_result:
        print("SUCCESS : correct value computed : computed_result = ", computed_result, " ; expected ", expected_result)
    else:
        print("FAILURE : wrong   value computed : computed_result = ", computed_result, " ; expected ", expected_result)


def common_calculation_part2(affected_points, santa_square_size):
    # display_affected_points(affected_points)
    width, height = len(affected_points[0]), len(affected_points)
    x_found, y_found = 0, 0
    first_is_found = False
    for it_y in range(0, height):
        for it_x in range(0, width):
            does_it_fit = does_santa_fit_at_position(affected_points, it_x, it_y, santa_square_size)
            # print("(x, y) = (", it_x, ", ", it_y, ") , does_it_fit = ", does_it_fit)
            if does_it_fit and not first_is_found:
                x_found, y_found = it_x, it_y
                first_is_found = True
    print("x_found = ", x_found, " ; y_found = ", y_found)
    return x_found * 10000 + y_found


def main():
    do_tests_part1()
    do_real_calculation_part1()
    do_tests_part2()
    do_real_calculation_part2()


if __name__ == "__main__":
    # execute only if run as a script
    main()

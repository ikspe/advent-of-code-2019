

test_01 = [
    """
         A           
         A           
  #######.#########  
  #######.........#  
  #######.#######.#  
  #######.#######.#  
  #######.#######.#  
  #####  B    ###.#  
BC...##  C    ###.#  
  ##.##       ###.#  
  ##...DE  F  ###.#  
  #####    G  ###.#  
  #########.#####.#  
DE..#######...###.#  
  #.#########.###.#  
FG..#########.....#  
  ###########.#####  
             Z       
             Z       
    """,
    23
]

test_02 = [
    """
                   A               
                   A               
  #################.#############  
  #.#...#...................#.#.#  
  #.#.#.###.###.###.#########.#.#  
  #.#.#.......#...#.....#.#.#...#  
  #.#########.###.#####.#.#.###.#  
  #.............#.#.....#.......#  
  ###.###########.###.#####.#.#.#  
  #.....#        A   C    #.#.#.#  
  #######        S   P    #####.#  
  #.#...#                 #......VT
  #.#.#.#                 #.#####  
  #...#.#               YN....#.#  
  #.###.#                 #####.#  
DI....#.#                 #.....#  
  #####.#                 #.###.#  
ZZ......#               QG....#..AS
  ###.###                 #######  
JO..#.#.#                 #.....#  
  #.#.#.#                 ###.#.#  
  #...#..DI             BU....#..LF
  #####.#                 #.#####  
YN......#               VT..#....QG
  #.###.#                 #.###.#  
  #.#...#                 #.....#  
  ###.###    J L     J    #.#.###  
  #.....#    O F     P    #.#...#  
  #.###.#####.#.#####.#####.###.#  
  #...#.#.#...#.....#.....#.#...#  
  #.#####.###.###.#.#.#########.#  
  #...#.#.....#...#.#.#.#.....#.#  
  #.###.#####.###.###.#.#.#######  
  #.#.........#...#.............#  
  #########.###.###.#############  
           B   J   C               
           U   P   P               
    """,
    58
]

test03 = [
    """
             Z L X W       C                 
             Z P Q B       K                 
  ###########.#.#.#.#######.###############  
  #...#.......#.#.......#.#.......#.#.#...#  
  ###.#.#.#.#.#.#.#.###.#.#.#######.#.#.###  
  #.#...#.#.#...#.#.#...#...#...#.#.......#  
  #.###.#######.###.###.#.###.###.#.#######  
  #...#.......#.#...#...#.............#...#  
  #.#########.#######.#.#######.#######.###  
  #...#.#    F       R I       Z    #.#.#.#  
  #.###.#    D       E C       H    #.#.#.#  
  #.#...#                           #...#.#  
  #.###.#                           #.###.#  
  #.#....OA                       WB..#.#..ZH
  #.###.#                           #.#.#.#  
CJ......#                           #.....#  
  #######                           #######  
  #.#....CK                         #......IC
  #.###.#                           #.###.#  
  #.....#                           #...#.#  
  ###.###                           #.#.#.#  
XF....#.#                         RF..#.#.#  
  #####.#                           #######  
  #......CJ                       NM..#...#  
  ###.#.#                           #.###.#  
RE....#.#                           #......RF
  ###.###        X   X       L      #.#.#.#  
  #.....#        F   Q       P      #.#.#.#  
  ###.###########.###.#######.#########.###  
  #.....#...#.....#.......#...#.....#.#...#  
  #####.#.###.#######.#######.###.###.#.#.#  
  #.......#.......#.#.#.#.#...#...#...#.#.#  
  #####.###.#####.#.#.#.#.###.###.#.###.###  
  #.......#.....#.#...#...............#...#  
  #############.#.#.###.###################  
               A O F   N                     
               A A D   M                     
"""
]


class Label:
    def __init__(self, x, y, label_name):
        self.x = x
        self.y = y
        self.label_name = label_name
        pass
    pass


class Maze:

    def __init__(self, lines_str):
        self.lines_str = lines_str
        self.height = len(self.lines_str)
        self.width = len(self.lines_str[0])
        self.labels = []
        self.portals = []
        self.start = None
        self.end = None
        pass

    @staticmethod
    def is_tile_wall(tile_value):
        if tile_value == '#':
            return True
        return False

    @staticmethod
    def is_tile_passage(tile_value):
        if tile_value == '.':
            return True
        return False

    @staticmethod
    def is_tile_label(tile_value):
        if tile_value == '.' or tile_value == '#' or tile_value == ' ':
            return False
        return True

    def register_label(self, x, y, label_name):
        # print("Found label : ", label_name, " (col ) at position : (x, y) = (", x, ", ", y, ")")
        label = Label(x, y, label_name)
        self.labels.append(label)

    def init_maze(self):
        if not self.init_compute_all_labels():
            return False
        if not self.init_compute_start_begin_and_portals():
            return False
        return True

    def init_compute_all_labels(self):
        for y in range(0, self.height):
            line = self.lines_str[y]
            for x in range(0, self.width):
                cur_tile = line[x]
                if not self.is_tile_label(cur_tile):
                    continue
                if x < self.width - 1:
                    next_tile_line = line[x+1]
                    if self.is_tile_label(next_tile_line):
                        label_name = cur_tile + next_tile_line
                        if x > 0:
                            prev_tile_line = line[x-1]
                            if self.is_tile_passage(prev_tile_line):
                                self.register_label(x - 1, y, label_name)
                                continue
                        if x < self.width - 2:
                            next_next_tile_line = line[x+2]
                            if self.is_tile_passage(next_next_tile_line):
                                self.register_label(x + 2, y, label_name)
                                continue
                        print("ERROR. Found label but no associated passage. Should not happen ! (line) : (x, y, name) = (", x, ", ", y, ", ", label_name, ")")
                        return False
                if y < self.height - 1:
                    next_tile_col = self.lines_str[y+1][x]
                    if self.is_tile_label(next_tile_col):
                        label_name = cur_tile + next_tile_col
                        if y > 0:
                            prev_tile_col = self.lines_str[y-1][x]
                            if self.is_tile_passage(prev_tile_col):
                                self.register_label(x, y-1, label_name)
                                continue
                        if y < self.height - 2:
                            next_next_tile_col = self.lines_str[y+2][x]
                            if self.is_tile_passage(next_next_tile_col):
                                self.register_label(x, y+2, label_name)
                                continue
                        print("ERROR. Found label but no associated passage. Should not happen ! (col ) : (x, y, name) = (", x, ", ", y, ", ", label_name, ")")
                        return False
        # print("Debug : success find_all_labels()")
        return True

    def init_compute_start_begin_and_portals(self):
        labels_copy = self.labels.copy()
        while not labels_copy == []:
            first_label = labels_copy[0]
            del labels_copy[0]
            if first_label.label_name == 'AA':
                self.start = first_label
                continue
            if first_label.label_name == 'ZZ':
                self.end = first_label
                continue
            found_output_label = None
            for it_label in labels_copy:
                if it_label.label_name == first_label.label_name:
                    found_output_label = it_label
                    labels_copy.remove(it_label)
                    break
            if found_output_label is None:
                print("ERROR. Not found output label for label ", first_label.label_name)
                return False
            # print("Debug : found portal : ", first_label.label_name, " between (", first_label.x, ", ", first_label.y, ") and (", found_output_label.x, ", ", found_output_label.y, ")")
            self.portals.append([first_label, found_output_label])
        # print("Debug : success find_start_begin_and_portals()")
        return True

    def is_it_the_end(self, x, y):
        if self.end.x == x and self.end.y == y:
            return True
        return False

    def is_it_a_portal(self, x, y):
        for it_portal in self.portals:
            for it_label in it_portal:
                if it_label.x == x and it_label.y == y:
                    copy_of_portal = it_portal.copy()
                    copy_of_portal.remove(it_label)
                    output_of_portal = copy_of_portal[0]
                    return True, output_of_portal
        return False, None

    def get_adjacent_tiles(self, x, y):
        candidates = [[x-1, y], [x+1, y], [x, y-1], [x, y+1]]
        final_tiles = []
        for it_tile in candidates:
            if it_tile[0] < 0 or it_tile[0] >= self.width or it_tile[1] < 0 or it_tile[1] >= self.height:
                continue
            if self.is_tile_passage(self.lines_str[it_tile[1]][it_tile[0]]):
                final_tiles.append(it_tile)
        return final_tiles

    def find_shortest_path(self):
        if self.start is None or self.end is None:
            print("ERROR : missing start or end")
            return False, []

        list_all_path_possible = []
        curr_path_tiles = []
        curr_path_tiles_choices_not_yet_tried = []  # list having same size as 'curr_path_tiles' but storing possible choices ; will be used to compute other possibilities

        curr_x, curr_y = self.start.x, self.start.y

        # this loop will try to find a path 5enclosed loo) ; when finished, jump back to previous intersection and try another path
        while True:

            # this loop will compute a path, or find a cul-de-sac
            we_have_a_path = False
            while True:
                curr_path_tiles.append([curr_x, curr_y])
                # print("find_shortest_path() ; curr_x, curr_y = ", curr_x, ", ", curr_y, "   ; curr_path_tiles = ", curr_path_tiles)
                if self.is_it_the_end(curr_x, curr_y):
                    # print("Found the end, stopping ; len(curr_path_tiles) = ", len(curr_path_tiles), " , curr_path_tiles = ", curr_path_tiles)
                    curr_path_tiles_choices_not_yet_tried.append([])
                    list_all_path_possible.append(curr_path_tiles.copy())
                    we_have_a_path = True
                    break
                adjacent_tiles = self.get_adjacent_tiles(curr_x, curr_y)
                is_portal, out_portal = self.is_it_a_portal(curr_x, curr_y)
                if is_portal:
                    # print("find_shortest_path() ; is_portal : jumping : label = ", out_portal.label_name)
                    adjacent_tiles.append([out_portal.x, out_portal.y])
                else:
                    _ = 0
                    # print("find_shortest_path() ; not a portal")
                # print("find_shortest_path() ; adjacent_tiles = ", adjacent_tiles)

                # remove tile not available (already in current path)
                adjacent_tiles_not_yet_tried = []
                for it_tile in adjacent_tiles:
                    if it_tile in curr_path_tiles:
                        continue
                    adjacent_tiles_not_yet_tried.append(it_tile)
                # print("find_shortest_path() ; adjacent_tiles_not_yet_tried = ", adjacent_tiles_not_yet_tried)
                if not adjacent_tiles_not_yet_tried:
                    # print("ERROR, no adjacent tile for (", curr_x, ", ", curr_y, ") ; curr_path_tiles = ", curr_path_tiles)
                    curr_path_tiles_choices_not_yet_tried.append([])
                    break
                # try first adjacent tiles but keep in memory there were a choice and we could have chosen something else...
                curr_x, curr_y = adjacent_tiles_not_yet_tried[0]
                curr_path_tiles_choices_not_yet_tried.append(adjacent_tiles_not_yet_tried[1:])

            # print("finished computing one_path or failure... : we_have_a_path = ", we_have_a_path, " ; need to jump back to previous intersection and try other possibilities... ")
            # print("curr_path_tiles = ", curr_path_tiles)
            # print("curr_path_tiles_choices_not_yet_tried = ", curr_path_tiles_choices_not_yet_tried)
            # print("len(curr_path_tiles) = ", len(curr_path_tiles))
            # print("len(curr_path_tiles_choices_not_yet_tried) = ", len(curr_path_tiles_choices_not_yet_tried))

            found_another_intersection = False
            while True:
                if len(curr_path_tiles_choices_not_yet_tried) == 0:
                    break
                if len(curr_path_tiles_choices_not_yet_tried[-1]) != 0:
                    found_another_intersection = True
                    break
                else:
                    del curr_path_tiles[-1]
                    del curr_path_tiles_choices_not_yet_tried[-1]

            if not found_another_intersection:
                # print("Debug : NOT found_another_intersection ==> finished : found all paths : need to find shortest")
                break

            # print("Debug : found_another_intersection ==> changing path")
            curr_x, curr_y = curr_path_tiles_choices_not_yet_tried[-1][0]
            del curr_path_tiles_choices_not_yet_tried[-1][0]

        if len(list_all_path_possible) == 0:
            return False, []

        shortest_path = list_all_path_possible[0]
        for it_path in list_all_path_possible[1:]:
            if len(it_path) < len(shortest_path):
                shortest_path = it_path

        return True, shortest_path
        pass  # find_shortest_path()

    pass


def extract_lines_from_multi_lines_str(str_input):
    tmp_all_lines = str_input.split('\n')
    return tmp_all_lines[1:-1]


def common_calculation_part1(lines_input, expected_path_len):
    my_maze = Maze(lines_input)
    if not my_maze.init_maze():
        print("ERROR. Impossible to init maze")
        return
    is_found_path, path_including_beginning = my_maze.find_shortest_path()
    if is_found_path:
        if expected_path_len == len(path_including_beginning[1:]):
            print("SUCCESS. Found. len = ", len(path_including_beginning[1:]), " ; expected_path_len = ", expected_path_len, " ; len(path_including_beginning) = ", len(path_including_beginning), " ; path_including_beginning = ", path_including_beginning)
        else:
            print("ERROR. Found but bad length. len = ", len(path_including_beginning[1:]), " ; expected_path_len = ", expected_path_len, " ; len(path_including_beginning) = ", len(path_including_beginning), " ; path_including_beginning = ", path_including_beginning)
    else:
        print("ERROR : Failure : not path found...")


def do_tests_part1():
    print("==== do_tests_part1() ====")
    all_tests_part1 = [test_01, test_02]
    for it_test in all_tests_part1:
        print("    ==== New Maze input")
        str_input = it_test[0]
        expected_path_len = it_test[1]
        print(str_input)
        lines_input = extract_lines_from_multi_lines_str(str_input)
        common_calculation_part1(lines_input, expected_path_len)


def do_real_calculation_part1():
    print("==== do_real_calculation_part1() ====")
    with open("input.txt") as file:  # Use file to refer to the file object
        lines_input = []
        line = file.readline()
        while line:
            lines_input.append(line.split('\n')[0])
            line = file.readline()
        expected_path_len = 666  # found value of part1 is 666, kept for non-regression
        common_calculation_part1(lines_input, expected_path_len)


def do_tests_part2():
    print("==== do_tests_part2() ====")
    print("""ERROR. Not implemented part 2. Need to change algorithm shortest path to remember : current layer level ;
        if layer not 0 : is_it_the_end() never true ;
        remember the label name and position when jumping through a portal. When using same portal label but different position : decrement layer
        remember also for each tile the layer, and instead of checking simply if possible to jump, need to check if the layer num was identical
        """)


def main():
    do_tests_part1()
    do_real_calculation_part1()
    # do_tests_part2()
    # do_real_calculation_part2()


if __name__ == "__main__":
    # execute only if run as a script
    main()
